all:
	rm -rf app
	gcc main.c command_line/command_interpreter.c network/udp_actions.c network/tcp_actions.c ui/ui.c files/files_manager.c utils/utils.c global_structures.c -o app -lcrypto -lssl -lcurses -pthread `pkg-config --cflags --libs glib-2.0`
